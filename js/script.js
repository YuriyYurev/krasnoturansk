$(function() {
  if(document.querySelector('header') || document.querySelector('.header')) {
    $(window).bind('scroll', function () {
      if ($(window).scrollTop() > $('header').height()) {
        $('main').css('padding-top', $('.header').height())
          $('.header').addClass('fixed');
      } else {
          $('.header').removeClass('fixed');
          $('main').css('padding-top', '')
      }
    });
  }
  //brand-slider
  (function workCheckbox() {
    if(document.querySelector('.field-radio') || document.querySelector('.field-checkbox')) {
      function changeActiveCheckbox() {
        $('.field-radio, .field-checkbox').each(function(i) {
          if($(this).children('input')[0].checked) {
            $(this).addClass('active')
            if($(this).parent('.politic')) {
              $(this).parent('.politic').removeClass('error')
            }
          } else {
            $(this).removeClass('active')
          }

        })
      }
      $('input[type="checkbox"], input[type="radio"]').on("input", function() {
        changeActiveCheckbox()
      })
      changeActiveCheckbox()
    }
  }());
  
  // brand-slider
  (function workSlider() {
    if(document.getElementById('sliderBrand')) {
      var sliderBrand = $("#sliderBrand")
    
      sliderBrand.slick({
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        speed: 100,
        prevArrow: $("#sliderBrandPrev"),
        nextArrow: $("#sliderBrandNext"),
        responsive: [
          {
            breakpoint: 1200,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 576,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              nav: false
            }
          }
        ]
      });
    }
    if (document.getElementById('sliderHero')) {
      var sliderHero = $('#sliderHero')
      sliderHero.find('.hero__wrap').slick({
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        speed: 400,
        dots: true,
        appendDots: sliderHero.find('.hero__dots')
      })
    }
  }());

  //aside
  (function workAside() {
    if(document.querySelector('.aside .current')) {
      $('.aside .current').click(function(){
        $(this).toggleClass('active')
      })
    };
    
  }());
  
  // tabs
  (function workTabs() {

    if(document.querySelector('.tabs .tabs__items')) {

      function hideTabContent(index, tab) {
        tab.find('.tabs__content').each(function(i) {
          if(index === i) {
            $(this).css('display', 'block')
          } else {
            $(this).css('display', 'none')
          }
        })
      } 

      function changeTabsActive(elem, tabs) {
        tabs.find('.tabs__items').children('button').each(function(i) {
          if(elem === $(this)[0]) {
            hideTabContent(i, tabs)
            $(this).addClass('active')
          } else {
            $(this).removeClass('active')
          }
        })
      }

      $('.tabs').on('click', function(event) {
        if(event.target.tagName === 'BUTTON') {
          changeTabsActive(event.target, $(this))
        }
      })
      $('.tabs').each(function() {
        hideTabContent(0, $(this))
      })
    };
  }());

  // accordeon
  (function workAccordeon() {
    if(document.querySelector('.accordeon__wrap .accordeon__item')) {
      function slideAccordeonItem() {
        $('.accordeon__wrap .accordeon__item').each(function() {
          if($(this).hasClass('active')) {
            $(this).children('.accordeon__content').slideDown()
          } else {
            $(this).children('.accordeon__content').slideUp()
          }
        })
      }
      slideAccordeonItem()
      $('.accordeon__wrap .accordeon__item .accordeon__action').each(function(i){
        $(this).click(function() {
          $(this).parent('.accordeon__item').toggleClass('active')
          slideAccordeonItem()
        })
      })
    };
  }());

  // kebabMenu
  (function workMenuKebab() {
    if(document.getElementById('menuKebab') && document.getElementById('iconKebab')) {
      var menuKebab = $("#menuKebab"),
          iconKebab = $("#iconKebab");

      function checkActiveMenuKebab() {
        if(iconKebab.hasClass("active")) {
          menuKebab.slideDown()
          $(document.body).addClass("overflow-hidden")
        } else {
          menuKebab.slideUp()
          $(document.body).removeClass("overflow-hidden")
        }
      }
      iconKebab.click(function() {
        $(this).toggleClass('active')
        checkActiveMenuKebab()
      })
      checkActiveMenuKebab()
    }
  }());

  // dropfile
  (function inputFile() {
    if (document.getElementById("loadFile") || document.getElementById("fileDrop")) {
      var renderList = function renderList(array) {
        if (array.length === 0) {
          fileDrop.html("");
        }
        var str = '';
        array.forEach(function (file, i, mass) {
          var sizeFile = file.size / 1024;
          var widthRange = file.size / 1024 / 1024 / maxSizeMB * 100;
    
          if (file.size / 1024 / 1024 > maxSizeMB) {
            $(".file-download").addClass("error");
            array.splice(i, 1);
          }
    
          str += "<div class=\"file__item\">\n<svg class=\"file__icon\" width=\"48\" height=\"48\" viewBox=\"0 0 48 48\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n<g clip-path=\"url(#clip0)\">\n<path d=\"M35.7178 6.96984L32.3536 0H8.06385C6.73035 0 5.64941 1.08103 5.64941 2.41453V45.5856C5.64941 46.919 6.73035 48 8.06385 48H36.6498C37.1786 48 37.6673 47.8296 38.0651 47.5413H40.0999V8.49694L35.7178 6.96984Z\" fill=\"#EAF6FF\"/>\n<path d=\"M40.1005 8.49707L39.0649 8.13623V9.99623V45.5858C39.0649 46.9193 37.9839 48.0003 36.6504 48.0003H39.9356C41.2691 48.0003 42.3501 46.9193 42.3501 45.5858V9.99614L40.1005 8.49707Z\" fill=\"#D8ECFE\"/>\n<path d=\"M36.5548 16.6391H11.444C11.044 16.6391 10.7197 16.3148 10.7197 15.9147C10.7197 15.5147 11.044 15.1904 11.444 15.1904H36.5547C36.9547 15.1904 37.279 15.5147 37.279 15.9147C37.279 16.3148 36.9548 16.6391 36.5548 16.6391Z\" fill=\"#82AEE3\"/>\n<path d=\"M36.5548 23.0775H11.444C11.044 23.0775 10.7197 22.7533 10.7197 22.3532C10.7197 21.9532 11.044 21.6289 11.444 21.6289H36.5547C36.9547 21.6289 37.279 21.9532 37.279 22.3532C37.279 22.7533 36.9548 23.0775 36.5548 23.0775Z\" fill=\"#82AEE3\"/>\n<path d=\"M36.5548 29.5165H11.444C11.044 29.5165 10.7197 29.1922 10.7197 28.7922C10.7197 28.3922 11.044 28.0679 11.444 28.0679H36.5547C36.9547 28.0679 37.279 28.3922 37.279 28.7922C37.2791 29.1922 36.9548 29.5165 36.5548 29.5165Z\" fill=\"#82AEE3\"/>\n<path d=\"M36.5548 35.955H11.444C11.044 35.955 10.7197 35.6307 10.7197 35.2307C10.7197 34.8306 11.044 34.5063 11.444 34.5063H36.5547C36.9547 34.5063 37.279 34.8306 37.279 35.2307C37.2791 35.6307 36.9548 35.955 36.5548 35.955Z\" fill=\"#82AEE3\"/>\n<path d=\"M34.768 9.996H42.3494L32.3535 0V7.58147C32.3535 8.91497 33.4345 9.996 34.768 9.996Z\" fill=\"#B3DAFE\"/>\n</g>\n<defs>\n<clipPath id=\"clip0\">\n<rect width=\"48\" height=\"48\" fill=\"white\"/>\n</clipPath>\n</defs>\n</svg>\n<div class=\"file__description\">\n<div class=\"file__between\">\n<span class=\"file__size\">".concat(file.name, "</span>\n<div class=\"file__close\">\n<span class=\"file__size\">").concat(sizeFile > 1000 ? "".concat((sizeFile / 1024).toFixed(2), " \u041C\u0411") : "".concat(sizeFile.toFixed(2), " \u041A\u0411"), "<i class=\"icon-close\"></i></span>\n</div>\n</div>\n<div class=\"file__progress\">\n<div class=\"file__progress_bar\" style=\"width: ").concat(widthRange, "%\"></div>\n</div>\n</div>\n</div>");
    
          if (i + 1 === mass.length) {
            fileDrop.html(str);
          }
        });
      };
    
      var arr = [];
      var maxSizeMB = 20;
      var fileDrop = $("#fileDrop");
      $("#loadFile").on('input', function (event) {
        $(".file-download").removeClass("error");
        Object.values(event.target.files).forEach(function (file) {
          arr.push(file);
          renderList(arr);
        });
      });
      $("#fileDrop").on("click", function (event) {
        if ($(event.target).hasClass('icon-close')) {
          $(".file-download").removeClass("error");
          arr = arr.filter(function (file) {
            return file.name !== $(event.target.closest(".file__description")).children('.file__between').children(".file__size").text();
          });
          renderList(arr);
        }
      });
      renderList(arr);
    }
  }());

  if(document.querySelector('input[type="tel"]')) {
    $('input[type="tel"]').each(function() {
      $(this).mask('+7(999) 999 99 99')
    })  
  }
  if(document.querySelector(".cookie")) {
    var cookie = $(".cookie")
    setTimeout(function() {
      cookie.css("display", "block");
    }, 3000)
      cookie.on("click", function(event) {
      if(event.target.closest('.cookie__close')) {
        $(this).css('display', "none")
      }
    })
  }
})